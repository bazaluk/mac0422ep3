#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <signal.h>
#include <unistd.h>
#include <time.h>
#include <readline/readline.h>
#include <readline/history.h>
#define LSH_TOK_DELIM " \t\r\n\a"
#define MAX_COMMAND_LENGTH 100
#define MAX_ARG_LENGTH 64
#define MAX_ARG_COUNT 5

#define FIM 25601 // indice que significa que o bloco fat esta ocupado e é final
#define K 25600 // quantidade de blocos total do sistema (em 100mb)
#define BLOCK_LENGHT 4000 // 4KB (um char ocupa 1 byte)
#define QTD_BLOCOS_BITMAP 7 // = teto(25600 / 4000)
#define QTD_BLOCOS_FAT 32 // = teto(128000 / 4000)
#define BLOCO_DIR_PADRAO (QTD_BLOCOS_BITMAP + QTD_BLOCOS_FAT)

unsigned int bitmap[K]; // vetor que guarda o bitmap
unsigned int fat[K];
FILE* fs_mount_point = NULL; // arquivo que representan nosso sistema de arquivos

// helper -> indica se o sistema de arquivos ja foi montado ou não.
int _is_mounted() {
	return fs_mount_point != NULL;
}

int get_bloco_inicio(int bloco) {
	return bloco * BLOCK_LENGHT;
}

void inicia_bitmap() {
	printf("Iniciando bitmap\n");
	int i;
	// inicia o bitmap com 1, que significa vazio
	for (int i = 0; i < K; i++) {
		bitmap[i] = 1;
	}

	// indica os blocos do bitmap e do fat como ocupados de inicio
	// eles estarão no começo do disco
	for (i = 0; i < QTD_BLOCOS_BITMAP + QTD_BLOCOS_FAT; i++) {
		bitmap[i] = 0;
	}
	bitmap[BLOCO_DIR_PADRAO] = 0;
}

void inicia_fat() {
	// Inicia todos os bloco fat como vazios
	int i;
	for (i = 0; i < K; i++) {
		fat[i] = 0;
	}

	// linka os blocos relacionas ao bit map
	for (i = 0; i < QTD_BLOCOS_BITMAP-1; i++) {
		fat[i] = i+1;
	}
	// indica ultimo bloco do bitmap
	fat[QTD_BLOCOS_BITMAP-1] = FIM;

	// linka os blocos relacionados ao fat
	// estarão logo após os blocos do bitmap
	for (i = QTD_BLOCOS_BITMAP; i < QTD_BLOCOS_BITMAP + QTD_BLOCOS_FAT-1; i++) {
		fat[i] = i+1;
	}
	// indica ultimo bloco do fat
	fat[QTD_BLOCOS_BITMAP + QTD_BLOCOS_FAT-1] = FIM;

	// Indica o diretorio padrao
	fat[BLOCO_DIR_PADRAO] = FIM;
}

void carrega_bitmap() {
	printf("--- Carregando bitmap\n");
	// colocando o bitmat do arquivo na memória.
	// como os bitmap está no começo do arquivo,
	// podemos carregar assim.
	char bit;
	fseek(fs_mount_point, 0, SEEK_SET);
	for (int i = 0; i < K; i++) {
		bit = (char)fgetc(fs_mount_point);
		if (bit == '0') {
			bitmap[i] = 0;
		}
		else {
			bitmap[i] = 1;
		}
	}
}

void carrega_fat() {
	printf("--- Carrega fat\n");
	// Coloco o ponteiro na posição depois do bitmap
	int pos = get_bloco_inicio(QTD_BLOCOS_BITMAP);
	char fat_value[6];
	int fat_int;
	fat_value[5] = '\0';

	fseek(fs_mount_point, pos, SEEK_SET);
	for (int i=0; i < K; i++) {
		size_t b_read = fread(&fat_value, 1, 5, fs_mount_point);

		fat_int = atoi(fat_value);
		fat[i] = fat_int;
	}
}

void escreve_bitmap() {
	// Como o bitmap está nos primeiros blocos, podemos escrever desse maneira
	// sem problemas
	char bit;
	int i;
	fseek(fs_mount_point, 0, SEEK_SET);
	for (i = 0; i < K; i++) {
		// mover o ponteiro para a posição i
		//
		fseek(fs_mount_point, i, SEEK_SET);
		bit = '0';
		if (bitmap[i] == 1) bit = '1';
		size_t q = fwrite(&bit, sizeof(char), 1, fs_mount_point);
	}
}

void escreve_fat() {
	// começa a escrever depois do bloco do bitmap
	int pos = get_bloco_inicio(QTD_BLOCOS_BITMAP);
	fseek(fs_mount_point, pos, SEEK_SET);
	for (int i = 0; i < K; i++){

		// 1 a mais que 5 por conta do caractere final de string
		char final_string[6];
		// converter o inteiro para string no formato do fat
		snprintf(final_string, 6, "%05d", fat[i]);

		size_t q = fwrite(&final_string, sizeof(char), 5, fs_mount_point);
	}
}

void cria_diretorio_padrao() {
	int pos = get_bloco_inicio(BLOCO_DIR_PADRAO);
	fseek(fs_mount_point, pos, SEEK_SET);
	char root[40];
	//DIR/|ultimo_acesso|ultim_modif|criado|<lista de onde comecam os arquivos>|arq1|# de bytes|ultimo_acesso|ultim_modif|criado|<conteudo>|arq2|# de bytes|ultimo_acesso|ultim_modif|criado|<conteudo>|DIR/<dir2>|dentro dir2|

	time_t ctime=time(NULL);
	snprintf(root, sizeof(root), "DIR/|%d|%d|%d||\0", ctime,ctime,ctime);
	size_t q = fwrite(&root, sizeof(char), sizeof(root), fs_mount_point);
}

void fs_mount(char* filename) {
	// Tenta abrir o arquivo com acesso de leitura e escrita.
	fs_mount_point = fopen(filename, "r+");
	if (_is_mounted()) {
		fseek(fs_mount_point, 0L, SEEK_END);
		long int file_size = ftell(fs_mount_point);
		fseek(fs_mount_point, 0, SEEK_SET);

		if (file_size > 0 ) { // Arquivo com conteúdo
			carrega_bitmap();
			carrega_fat();
		}
		else { // arquivo sem conteudo
			inicia_bitmap();
			inicia_fat();
		}
	}
	else {
		// Caso não seja possivel, então cria um novo, com acesso de escrita
		fs_mount_point = fopen(filename, "w+");
		printf("não consegui abrir o arquivo, criando um novo..\n");
		inicia_bitmap();
		inicia_fat();
		cria_diretorio_padrao();
	}
}

void fs_umount() {
	printf("Umounting..\n");
	if (_is_mounted()) {
		escreve_bitmap();
		escreve_fat();
		fclose(fs_mount_point);
		printf("Sistema de arquivos desmontado\n");
	}
	else {
		printf("Monte o sintema de arquivos primeiro\n");
	}
} 


int parse_input(char input[MAX_COMMAND_LENGTH], char arguments[][MAX_ARG_LENGTH]) {
	int i = 0;
	int slot = 0;
	int j = 0;
	// Reset arguments..
	arguments[0][0] = '\0';
    while (input[i] != '\0') {
		if (input[i] == ' ') {
			arguments[slot][j] = '\0';
			slot++;
			j = 0;
		}
		else if (input[i] == '\n') { }
		else {
			arguments[slot][j] = input[i];
			j++;
		}
		i++;
	}
	//Garente que a última execução tenha um indicador de final de string..
	arguments[slot][j] = '\0';
	return slot;
}

void exec_command(char arguments[][MAX_ARG_LENGTH], int argc) {
	int i;
	if (strcmp(arguments[0],"kill") == 0) {
		int pid = atoi(arguments[2]);
		kill(pid, 9);
	}
	else if (strcmp(arguments[0], "mkdir") == 0) {
		mkdir(arguments[1], 0755);
	}
	else if (strcmp(arguments[0], "ln") == 0) {
		symlink(arguments[2], arguments[3]);
	}		
	else if (strcmp(arguments[0], "mount") == 0) {
		fs_mount(arguments[1]);
	}
	else if (strcmp(arguments[0], "umount") == 0) {
		fs_umount(arguments[1]);
	}
	else{ //executar algum binario
		char **args;
		args=(char **)malloc((MAX_ARG_COUNT+1)*sizeof(char *));
		for (i=0;i<argc+1;i++){
			args[i]=arguments[i];
		}
		args[i]=NULL;
		pid_t pid=fork();
		if (pid==0) { // child process 
			execve(args[0],args,NULL);
		}
		else { // pid!=0; parent process 
			waitpid(pid,0,0); // wait for child to exit 
		}
	}

}

int main(int argc, char *argv[])
{
	//fs_mount("/home/bruna/usp/so/daniel/mac0422ep3/fs");
	//fs_umount();

	char com[MAX_COMMAND_LENGTH];
	char pwd[MAX_COMMAND_LENGTH];
	char arguments[MAX_ARG_COUNT][MAX_ARG_LENGTH];
	//pthread_t pid;
	using_history();

	getcwd(pwd,MAX_COMMAND_LENGTH);
	while(1){	
		printf("[ep3] ");
		char *com = readline(NULL);

		//arrow_hist(com);
		int p = parse_input(com, arguments);
		exec_command(arguments,p);
		add_history(com);
		// 
		com[0] = '\0';

		free(com);
	}

	clear_history();

	return 0;	
};

