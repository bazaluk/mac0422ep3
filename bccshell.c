#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <signal.h>
#include <unistd.h>
#include <readline/readline.h>
#include <readline/history.h>
#define LSH_TOK_DELIM " \t\r\n\a"
#define MAX_COMMAND_LENGTH 100
#define MAX_ARG_LENGTH 64
#define MAX_ARG_COUNT 5

void *arrow();
void arrow_hist(char * s);
////////////////////////////////////////////////////////
//TEM Q POR -LREADLINE PRA COMPILAR NAO ESQUECE


int parse_input(char input[MAX_COMMAND_LENGTH], char arguments[][MAX_ARG_LENGTH]) {
	int i = 0;
	int slot = 0;
	int j = 0;
	// Reset arguments..
	arguments[0][0] = '\0';
    while (input[i] != '\0') {
		if (input[i] == ' ') {
			arguments[slot][j] = '\0';
			slot++;
			j = 0;
		}
		else if (input[i] == '\n') { }
		else {
			arguments[slot][j] = input[i];
			j++;
		}
		i++;
	}
	//Garente que a última execução tenha um indicador de final de string..
	arguments[slot][j] = '\0';
	return slot;
}

void exec_command(char arguments[][MAX_ARG_LENGTH], int argc) {
	int i;
	if (strcmp(arguments[0],"kill") == 0) {
		int pid = atoi(arguments[2]);
		kill(pid, 9);
	}
	else if (strcmp(arguments[0], "mkdir") == 0) {
		mkdir(arguments[1], 0755);
	}
	else if (strcmp(arguments[0], "ln") == 0) {
		symlink(arguments[2], arguments[3]);
	}		

	else{ //executar algum binario
		char **args;
		args=(char **)malloc((MAX_ARG_COUNT+1)*sizeof(char *));
		for (i=0;i<argc+1;i++){
			args[i]=arguments[i];
		}
		args[i]=NULL;
		pid_t pid=fork();
		if (pid==0) { // child process 
			execve(args[0],args,NULL);
		}
		else { // pid!=0; parent process 
			waitpid(pid,0,0); // wait for child to exit 
		}
	}

}

int main(int argc, char *argv[])
{
	char com[MAX_COMMAND_LENGTH];
	char pwd[MAX_COMMAND_LENGTH];
	char arguments[MAX_ARG_COUNT][MAX_ARG_LENGTH];
	//pthread_t pid;
	using_history();

	getcwd(pwd,MAX_COMMAND_LENGTH);
	while(1){	
		printf("{%s@%s} ",getenv("USER"),pwd);
		char *com = readline(NULL);

		//arrow_hist(com);
		int p = parse_input(com, arguments);
		exec_command(arguments,p);
		add_history(com);
		// 
		com[0] = '\0';

		free(com);
	}

	clear_history();

	return 0;	
};

